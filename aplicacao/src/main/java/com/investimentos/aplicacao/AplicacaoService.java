package com.investimentos.aplicacao;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Optional;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AplicacaoService {
	@Autowired
	private AplicacaoRepository aplicacaoRepository;
	
	@Autowired
	private TransacaoRepository transacaoRepository;
	
	@Autowired
	private ClienteService clienteService;
	
	@Autowired
	private ProdutoService produtoService;
	
	public Optional<Investimento> criar(Investimento investimento) {
		Optional<Cliente> clienteOptional = clienteService.consultar(investimento.getCliente().getCpf());
		
		if(!clienteOptional.isPresent()) {
			return Optional.empty();
		}
		
		investimento.setCliente(clienteOptional.get());
	
		Optional<Produto> produtoOptional = produtoService.consultar(investimento.getProduto().getId());
		
		if(!produtoOptional.isPresent()) {
			return Optional.empty();
		}
		
		investimento.setProduto(produtoOptional.get());
		
		Aplicacao aplicacao = salvarAplicacao(investimento);
		investimento.setAplicacao(aplicacao);
		
		Transacao transacao = salvarTransacao(investimento);
		investimento.setTransacao(transacao);
		
		return Optional.of(investimento);
	}
		
	private Aplicacao salvarAplicacao(Investimento investimento) {
		Aplicacao aplicacao = new Aplicacao();
		
		aplicacao.setClienteCpf(investimento.getCliente().getCpf());
		aplicacao.setProdutoId(investimento.getProduto().getId());
		aplicacao.setSaldo(investimento.getTransacao().getValor());
		aplicacao.setDataCriacao(LocalDate.now());
		
		return aplicacaoRepository.save(aplicacao);
	}
	
	private Transacao salvarTransacao(Investimento investimento) {
		Transacao transacao = investimento.getTransacao();
		
		transacao.setId(UUID.randomUUID());
		transacao.setAplicacao(investimento.getAplicacao());
		transacao.setTimestamp(LocalDateTime.now());
		
		return transacaoRepository.save(transacao);
	}
}
